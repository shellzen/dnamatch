#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Pylint sie upiera ze tu powinien byc docstring.

Pewnie ma racje, ale nie wiem co tu wpisać. Jakiś copyright?
"""

import curses
import signal
import time
import sys
import os.path

from enum import Enum

class Colors(Enum):
    """Numery par kolorow"""
    ADENINE = 1
    GUANINE = 2
    CYTOSINE = 3
    THYMINE = 4
    BACKGROUND = 5
    WHITE_ON_WHITE = 13

class GAME_state:

    """Aktualny stan gry"""

    def max_x(self):
        return len(self.samples[0])-1
    def max_y(self):
        return len(self.samples)-1

class game:

    """Mechanika dzialania gry"""
    levels = []
    cur_level = -1
    position_x = 0
    position_y = 0
    grabbed_status = 0
    rewards = [
        ["       ",
         "       ",
         "       ",
         "       "],
        ["    1  ",
         "       ",
         "       ",
         "       "],
        ["    1  ",
         "      1",
         "1      ",
         "       "],
        ["    1  ",
         "  1   1",
         "1     1",
         "  1 1 1"],
        ]
    reward_level = 0

    def load_level(self, level):
        """wczytuje poziom"""
        self.cur_level = 0
        self.levels.append(level)

    def cur_state(self):
        """Aktualny poziom"""
        return self.levels[self.cur_level]

    def nextlevel(self):
        """Zmienia poziom"""
        self.position_x = 0
        self.position_y = 0
        self.grabbed_status = 0
        if self.cur_level < len(self.rewards)-1:
            self.cur_level += 1

    def win_check(self):
        """Sprawdza czy ilosc polaczen jest wystarczajaca do zakonczenia poziomu"""
        if self.cur_match() >= self.cur_state().min_match:
            self.reward_level += 1
            draw_reward()
            MAIN_BOX.clear()
            draw_main_header(MAIN_BOX)
            MAIN_BOX.addstr(5, 11, lang("MAIN_BOX.popup.stage"))
            MAIN_BOX.refresh()
            time.sleep(2)

            self.nextlevel()

    def cur_match(self):
        """Sprawdza ilosc polaczen"""
        match = 0
        total_columns = self.cur_state().max_x()+1

        for k in range(total_columns):
            if self.column_matched(k):
                match += 1

        return match

    def column_matched(self, i):
        total_lines_pairs = self.cur_state().max_y()
        pairs_match = 0
        for j in range(total_lines_pairs):
            if self.chrom_match(self.cur_state().samples[j][i], self.cur_state().samples[j+1][i]):
                pairs_match += 1
        return pairs_match == total_lines_pairs

    def chrom_match(self, c, x):
        return (c == x) and (c != "0")

    def grab_chromosome(self):
        """Chwyta chromosom i pozwala go przesuwac"""
        if self.cur_state().samples[self.position_y][self.position_x] != "0":
            self.grabbed_status = 1

    def release_chromosome(self):
        """Puszcza chromosom"""
        self.grabbed_status = 0
        self.win_check()

    def left_to_me(self):
        """wspolrzedna x pola na lewo od aktualnego polozenia kursora"""
        return self.get_my_line()[self.position_x - 1]

    def right_to_me(self):
        """Wspolrzedna x pola na prawo od aktualnego polozenia kursora"""
        return self.get_my_line()[self.position_x + 1]

    def get_me(self):
        """Wspolrzedna x aktualnego polozenia kursora"""
        return self.get_my_line()[self.position_x]

    def get_my_line(self):
        """Wspolrzedna y aktualnego polozenia kursora"""
        return self.cur_state().samples[self.position_y]

    def swap_left(self):
        """Przesuniecie chwyconego chromosomu o jedno pole na lewo"""
        if self.position_x > 0 and self.left_to_me() == "0":
            self.get_my_line()[self.position_x - 1] = self.get_me()
            self.get_my_line()[self.position_x] = "0"
            self.position_x -= 1

    def swap_right(self):
        """Przesuniecie chwyconego chromosomu o jedno pole na prawo"""
        if self.position_x < self.cur_state().max_x() and self.right_to_me() == "0":
            self.get_my_line()[self.position_x + 1] = self.get_me()
            self.get_my_line()[self.position_x] = "0"
            self.position_x += 1

global CUR_LANG

CUR_LANG = 'pl'
language = {
    'pl' : {
        'STATUS_BOX.sample':
            "Zestaw próbek:        ",
        'STATUS_BOX.min_match':
            "Wymagane dopasowanie: ",
        'STATUS_BOX.cur_match':
            "Aktualne dopasowanie: ",
        'STATUS_BOX.help' :
            "Użyj strzałek aby przesuwać chromosomy",
        'STATUS_BOX.help2' :
            "Wciśnij SPACEBAR aby chwycić chromosom",
        'MAIN_BOX.header.title':
            "DNA analizator v1.3                                                                         własność MSP",
        'MAIN_BOX.popup.exit' :
            "Eksperyment zakończony",
        'MAIN_BOX.popup.stage' :
            "Odnaleziono wzorzec dopasowania",
        'REWARD_BOX.title':
            "Macierz wyniku",
        'LOGIN_BOX.title':
            "Odblokuj system",
        'LOGIN_BOX.password':
            "Wprowadź hasło:",
        'LOCK_BOX.error_topic':
            "Brak danych z Wirówki!",
        'LOCK_BOX.error_desc':
            "Umieść prubówki w celu dokończenia eksperymentu",
        'LOCK_BOX.error_desc_full':
            "Umieść pozostałe prubówki aby dokończyć eksperyment"

        },
    'en' : {
        'STATUS_BOX.sample' :
            "Samples set:          ",
        'STATUS_BOX.min_match' :
            "Min match:            ",
        'STATUS_BOX.cur_match' :
            "Current match:        ",
        'STATUS_BOX.help' :
            "Use arrow keys to move chromosomes",
        'STATUS_BOX.help2' :
            "Hit SPACEBAR to grab chromosome",
        'MAIN_BOX.header.title' :
            "DNA comparator v1.3                                                                      property of MRP",
        'MAIN_BOX.popup.exit':
            "Experiment finished",
        'MAIN_BOX.popup.stage':
            "Sufficient match found",
        'REWARD_BOX.title' :
            "Results matrix",
        'LOGIN_BOX.title' :
            "Unlock the system",
        'LOGIN_BOX.password' :
            "Enter password:",
        'LOCK_BOX.error_topic':
            "Missing data from the experiments!",
        'LOCK_BOX.error_desc':
            "Find sample tubes to finish experiment",
        'LOCK_BOX.error_desc_full':
            "Find all 4 sample tubes to finish experiment"
        }
    }

def lang(key):
    """Aktualny jezyk"""
    return language[CUR_LANG][key]

def draw_screen():
    """Rysuje glowny ekran, na ktorym nie ma nic poza ramka"""
    BORDER_BOX.clear()
    BORDER_BOX.border(0)
    #BORDER_BOX.addstr(0,10,"Hit 'q' to quit")


    STD_SCR.refresh()
    BORDER_BOX.refresh()
    #draw_screen musi byc wywolywany przed draw_reward, inaczej go nadpisuje



def draw_reward():
    """Rysuje okno z nagroda za przejscie gry"""
    REWARD_BOX.box()
    REWARD_BOX.addstr(0, 3, lang("REWARD_BOX.title"))
    reward = CUR_GAME.rewards[CUR_GAME.reward_level];
    for i, v in enumerate(reward):
        draw_reward_line(REWARD_BOX, 4*i+2, 3, reward[i])
    REWARD_BOX.refresh()

def draw_reward_line(rbox, x, y, line):
    """Rysuje kolejne linie nagrody"""
    for i, c in enumerate(line):
        color = reward_char_color(c)
        rbox.addstr(x, y+(4*i)-1, c, color)
        rbox.addstr(x, y+(4*i), c, color)
        rbox.addstr(x, y+(4*i)+1, c, color)
        rbox.addstr(x, y+(4*i)+2, c, color)
        rbox.addstr(x+1, y+(4*i)-1, c, color)
        rbox.addstr(x+1, y+(4*i), c, color)
        rbox.addstr(x+1, y+(4*i)+1, c, color)
        rbox.addstr(x+1, y+(4*i)+2, c, color)

def reward_char_color(c):
    """Zwraca kolor nagrody"""
    if c != " ": # ulgy
        return curses.color_pair(Colors.WHITE_ON_WHITE.value)
    return 0

def draw_main_header(MAIN_BOX):
    MAIN_BOX.addstr(0, 2, lang("MAIN_BOX.header.title"))
    MAIN_BOX.addstr(2, 2, "Adenine", main_char_color("A"))
    MAIN_BOX.addstr(2, 33, "Thymine", main_char_color("T"))
    MAIN_BOX.addstr(2, 66, "Cytosine", main_char_color("C"))
    MAIN_BOX.addstr(2, 99, "Guanine", main_char_color("G"))

def game_ended():
    return CUR_GAME.reward_level >= len(CUR_GAME.rewards) -1;

def game_halve_ended():
    return CUR_GAME.reward_level >= len(CUR_GAME.rewards) -2;

def draw_main():
    """Rysuje glowne okno gry"""

    MAIN_BOX.clear()
    draw_main_header(MAIN_BOX)


    line_height = 2
    line_width = 6

    #MAIN_BOX.box()
    if not game_ended():
        samplesLineOffset = 4 + (4 - len(CUR_GAME.cur_state().samples))
        for j in range(len(CUR_GAME.cur_state().samples)):
            MAIN_BOX.addstr(j*line_height+samplesLineOffset, 2, "S"+str(j+1)+":")
            for i in range(len(CUR_GAME.cur_state().samples[j])):
                draw_main_line(MAIN_BOX, j*line_height+samplesLineOffset, 8 + i*line_width -2, "---")
                if CUR_GAME.cur_state().samples[j][i] != "0":
                    draw_main_line(MAIN_BOX, j*line_height+samplesLineOffset, 8 + i*line_width, CUR_GAME.cur_state().samples[j][i])
                else:
                    draw_main_line(MAIN_BOX, j*line_height+samplesLineOffset, 8 + i*line_width, "---")
                draw_main_line(MAIN_BOX, j*line_height+samplesLineOffset, 8 + i*line_width +1, "---")

                # comment out this block if game is to easy
                if j < CUR_GAME.cur_state().max_y():
                    if CUR_GAME.column_matched(i):
                        draw_main_line(MAIN_BOX, (j*line_height+samplesLineOffset)+1, 8 + i*line_width, "|")


        if CUR_GAME.grabbed_status == 0:
            MAIN_BOX.addstr(CUR_GAME.position_y * line_height + samplesLineOffset, CUR_GAME.position_x * line_width + 8 + 1, "|")
            MAIN_BOX.addstr(CUR_GAME.position_y * line_height + samplesLineOffset, CUR_GAME.position_x * line_width + 8 - 1, "|")

        else:
            MAIN_BOX.addstr(CUR_GAME.position_y * line_height + samplesLineOffset, CUR_GAME.position_x * line_width + 8 + 1, "]")
            MAIN_BOX.addstr(CUR_GAME.position_y * line_height + samplesLineOffset, CUR_GAME.position_x * line_width + 8 - 1, "[")
    else:
        MAIN_BOX.addstr(5, 11, lang("MAIN_BOX.popup.exit"))

    MAIN_BOX.refresh()

def draw_main_line(mbox, x, y, line):
    """Rysuje kolejne nici chromosomow"""
    for i, c in enumerate(line):
        mbox.addstr(x, y+i, c, main_char_color(c))


def main_char_color(c):
    """Zwraca kolory chromosomow"""
    if c == "A":
        return curses.color_pair(Colors.ADENINE.value)
    elif c == "T":
        return curses.color_pair(Colors.THYMINE.value)
    elif c == "C":
        return curses.color_pair(Colors.CYTOSINE.value)
    elif c == "G":
        return curses.color_pair(Colors.GUANINE.value)
    else:
        return 0

def draw_status():
    """Rysuje okno statusu"""
    #STATUS_BOX.box()

    if CUR_GAME.cur_level  < 3:
        STATUS_BOX.addstr(0, 2, lang("STATUS_BOX.sample") + str(CUR_GAME.cur_level +1) + "/3")
        STATUS_BOX.addstr(1, 2, lang("STATUS_BOX.min_match") + str(CUR_GAME.cur_state().min_match))
        STATUS_BOX.addstr(2, 2, lang("STATUS_BOX.cur_match") + str(CUR_GAME.cur_match()))
        STATUS_BOX.addstr(4, 2, lang("STATUS_BOX.help"))
        STATUS_BOX.addstr(5, 2, lang("STATUS_BOX.help2"))

    x_offset = 7
    y_offset = 3
    STATUS_BOX.addstr(1+x_offset, y_offset, "-._    _.--'\"`'--._    _.--'\"`'--._    _.--'\"`'--._    _  ")
    STATUS_BOX.addstr(2+x_offset, y_offset, "  '-:`.'|`|\"':-.  '-:`.'|`|\"':-.  '-:`.'|`|\"':-.  '.` : '.   ")
    STATUS_BOX.addstr(3+x_offset, y_offset, "'.  '.  | |  | |'.  '.  | |  | |'.  '.  | |  | |'.  '.:   '.  '.")
    STATUS_BOX.addstr(4+x_offset, y_offset, ": '.  '.| |  | |  '.  '.| |  | |  '.  '.| |  | |  '.  '.  : '.  `.")
    STATUS_BOX.addstr(5+x_offset, y_offset, "'   '.  `.:_ | :_.' '.  `.:_ | :_.' '.  `.:_ | :_.' '.  `.'   `.")
    STATUS_BOX.addstr(6+x_offset, y_offset, "       `-..,..-'       `-..,..-'       `-..,..-'       `         `")

    #STATUS_BOX.addstr(5, 2, "resetme: " + str(os.path.isfile("resetme")))

    STATUS_BOX.refresh()

def wir_locked():
    # check gpio without libs
    if os.path.exists("/sys/class/gpio/gpio475/value"):
        status = os.popen('cat /sys/class/gpio/gpio475/value').read()
        if "1" in status:
            return False

    return True

def wir_full():
    # check gpio without libs
    if os.path.exists("/sys/class/gpio/gpio476/value"):
        status = os.popen('cat /sys/class/gpio/gpio476/value').read()
        if "1" in status:
            return False

    return True

signal.signal(signal.SIGINT, signal.SIG_IGN)  # ctrl + c
signal.signal(signal.SIGTSTP, signal.SIG_IGN) # ctrl + z


# POZIOMY
CUR_GAME = game()
LEVEL_1 = GAME_state()
LEVEL_1.samples = [
    ["0", "0", "C", "0", "A", "0", "0", "0", "A", "0", "0", "0", "0", "0", "0", "0"],
    ["0", "C", "0", "0", "A", "0", "0", "A", "0", "0", "0", "0", "0", "0", "0", "0"]
    ]
LEVEL_1.min_match = 3

LEVEL_2 = GAME_state()
LEVEL_2.samples = [
    ["0", "A", "0", "A", "G", "0", "C", "0", "T", "0", "T", "0", "0", "0", "0", "0"],
    ["0", "A", "0", "G", "A", "0", "A", "C", "0", "T", "0", "0", "0", "0", "0", "0"],
    ["0", "A", "0", "G", "A", "0", "A", "C", "T", "0", "0", "0", "0", "0", "0", "0"]
    ]
LEVEL_2.min_match = 4

LEVEL_3 = GAME_state()
LEVEL_3.samples = [
    ["C", "G", "0", "G", "0", "0", "T", "0", "G", "T", "G", "0", "0", "0", "0", "0"],
    ["0", "A", "0", "0", "C", "0", "A", "T", "0", "G", "C", "0", "0", "0", "0", "0"],
    ["C", "A", "0", "G", "0", "A", "0", "T", "0", "A", "G", "0", "0", "0", "0", "0"]
    ]
LEVEL_3.min_match = 3

CUR_GAME.load_level(LEVEL_1)
CUR_GAME.load_level(LEVEL_2)
CUR_GAME.load_level(LEVEL_3)

WINDOW_SIZE = os.get_terminal_size()
LEFT_MARGIN = 11

STD_SCR = curses.initscr()
REWARD_BOX = curses.newwin(17, 33, 15, 75    + LEFT_MARGIN)
MAIN_BOX = curses.newwin(11, 106, 2, 2 + LEFT_MARGIN)
STATUS_BOX = curses.newwin(15, 70, 16, 2 + LEFT_MARGIN)
LOGIN_BOX = curses.newwin(6, 30, 14, 60 + LEFT_MARGIN)
BORDER_BOX = curses.newwin(33, 112, 0, LEFT_MARGIN)

#KOLORY
curses.start_color()
curses.init_pair(Colors.ADENINE.value, 233, 119)
curses.init_pair(Colors.GUANINE.value, 233, 45)
curses.init_pair(Colors.CYTOSINE.value, 233, 200)
curses.init_pair(Colors.THYMINE.value, 233, 135)
curses.init_pair(Colors.BACKGROUND.value, 230, 233)
curses.init_pair(Colors.WHITE_ON_WHITE.value, 255, 255)
curses.use_default_colors()

#STD_SCR.bkgd(' ', curses.color_pair(Colors.BACKGROUND.value))
BORDER_BOX.bkgd(' ', curses.color_pair(Colors.BACKGROUND.value))
MAIN_BOX.bkgd(' ', curses.color_pair(Colors.BACKGROUND.value))
REWARD_BOX.bkgd(' ', curses.color_pair(Colors.BACKGROUND.value))
STATUS_BOX.bkgd(' ', curses.color_pair(Colors.BACKGROUND.value))
LOGIN_BOX.bkgd(' ', curses.color_pair(Colors.BACKGROUND.value))


curses.cbreak()
curses.noecho()
STD_SCR.keypad(1)
curses.curs_set(False)


alert = [
    "                         .i;;;;i.",
    "                       iYcviii;vXY:",
    "                    .YC.     .    in7.",
    "                   .vc.   ......   ;1c.",
    "                   i7,   ..        .;1;",
    "                  i7,   .. ...      .Y1i",
    "                 ,7v     .6MMM@;     .YX,",
    "                .7;.   ..IMMMMMM1     :t7.",
    "               .;Y.     ;$MMMMMM9.     :tc.",
    "               vY.   .. .nMMM@MMU.      ;1v.",
    "              i7i   ...  .#MM@M@C. .....:71i",
    "             it:   ....   $MMM@9;.,i;;;i,;tti",
    "            :t7.  .....   0MMMWv.,iii:::,,;St.",
    "           .nC.   .....   IMMMQ..,::::::,.,czX.",
    "          .ct:   ....... .ZMMMI..,:::::::,,:76Y.",
    "          c2:   ......,i..Y$M@t..:::::::,,..inZY",
    "         vov   ......:ii..c$MBc..,,,,,,,,,,..iI9i",
    "        i9Y   ......iii:..7@MA,..,,,,,,,,,....;AA:",
    "       iIS.  ......:ii::..;@MI....,............;Ez.",
    "      .I9.  ......:i::::...8M1..................C0z.",
    "     .z9;  ......:i::::,.. .i:...................zWX.",
    "     vbv  ......,i::::,,.      ................. :AQY",
    "    c6Y.  .,...,::::,,..:t0@@QY. ................ :8bi",
    "   :6S. ..,,...,:::,,,..EMMMMMMI. ............... .;bZ,",
    "  :6o,  .,,,,..:::,,,..i#MMMMMM#v.................  YW2.",
    " .n8i ..,,,,,,,::,,,,.. tMMMMM@C:.................. .1Wn ",
    " 7Uc. .:::,,,,,::,,,,..   i1t;,..................... .UEi",
    " 7C...::::::::::::,,,,..        ....................  vSi. ",
    " ;1;...,,::::::,.........       ..................    Yz:  ",
    "   izAotX7777777777777777777777777777777777777777Y7n92:    ",
    "     .;CoIIIIIUAA666666699999ZZZZZZZZZZZZZZZZZZZZ6ov.      "]


lamp = [
"                            dMMMMMMMMm-",
"                      .:-  `NMMMMMMMMMN:",
"                  -/ymdydd--NMMMMMMMmddy/:-",
"              -/ymdyo+/:.dNdhMMmddddNNMMMMNms-",
" `-+ydhyo++/-.-:/++ohdhs/.`yMMMMMMMMMMMMMMMMMMMMMo",
":mhs+++/-.-:/++shdhs/.`   `NMMMMMMMMMMMMMMMMMMMMMN:",
"sM/`y:::/++shdhs/.`       `NMMMMMMMMMMMMMMMMNdy+:.`",
"`dm--/+shdhs:.`            mMMMMMMMMMMMMNNMh.`",
" .ydhhmMyso/.              oMMMMMMmdMMdsomm:`. ",
"   .-+dNds+hm:             `mmdy+-.`:syyy+. `:/",
"    `Nd.:+/:hN:             ..`    `+`   /`",
"     `yN+o:`o:hN:                  `      . ",
"      `yN/o-`o:hN: ",
"       `yN/o-`s:hN:",
"        `yN/o-`s:hN-",
"         `yN/s-`s-hN-",
"          `yN/s-`s-hN-",
"           `yN/o:+:`dN-",
"        `````hN/.-``.dN-```````",
"      :hhhhhhdMNhhhhhmMmhhhhhhhy-",
"      hMMMMMMMMMMMMMMMMMMMMMMMMMs ",
"    `.-syyyyyyyyyyyyyyyyyyyyyyyo-.`",
"   `mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd",
"    .-----------------------------`",
]

def clear_screen():
    for i in range(31):
        BORDER_BOX.addstr(i+1, 1, " "*110)

    CUR_GAME.release_chromosome()
    BORDER_BOX.refresh()

def allow_input():
    draw_screen()
    draw_reward()
    draw_main()
    draw_status()

    key = STD_SCR.getch()

    #PRAWO
    if key == curses.KEY_RIGHT:
        if CUR_GAME.position_x < CUR_GAME.cur_state().max_x():
            if CUR_GAME.grabbed_status == 1:
                CUR_GAME.swap_right()
            else:
                CUR_GAME.position_x += 1

    #LEWO
    elif key == curses.KEY_LEFT:
        if CUR_GAME.position_x > 0:
            if CUR_GAME.grabbed_status == 1:
                CUR_GAME.swap_left()
            else:
                CUR_GAME.position_x -= 1

    #GORA
    elif key == curses.KEY_UP:
        if CUR_GAME.position_y > 0 and CUR_GAME.grabbed_status == 0:
            CUR_GAME.position_y -= 1
    #DOL
    elif key == curses.KEY_DOWN:
        if CUR_GAME.position_y < CUR_GAME.cur_state().max_y() and CUR_GAME.grabbed_status == 0:
            CUR_GAME.position_y += 1
    #SPACJA
    elif key == ord(' '):
        if CUR_GAME.grabbed_status == 0:
            CUR_GAME.grab_chromosome()
        else:
            CUR_GAME.release_chromosome()
    #toggle language
    elif key == ord('&'):
        d = {"pl":"en", "en":"pl"}
        global CUR_LANG
        CUR_LANG = d[CUR_LANG]
    # quit
    elif key == ord('*'):
        sys.exit(1)



try:
    draw_screen()

    clear_screen()

    while wir_locked() and not os.path.isfile("/tmp/wirok"):
        BORDER_BOX.addstr(6, 65, lang("LOCK_BOX.error_topic"))
        BORDER_BOX.addstr(7, 46, lang("LOCK_BOX.error_desc"))
        for i, line in enumerate(alert):
            BORDER_BOX.addstr(1+i, 3, line)
        BORDER_BOX.refresh()

    clear_screen()

    password = " "
    while password != "43288" and not os.path.isfile("/tmp/nolock"):
        for i, line in enumerate(lamp):
            BORDER_BOX.addstr(4+i, 1, line)
        BORDER_BOX.refresh()

        LOGIN_BOX.box()
        LOGIN_BOX.addstr(0, 1, lang('LOGIN_BOX.title'))
        LOGIN_BOX.addstr(2, 3, lang('LOGIN_BOX.password'))
        curses.echo()
        password = LOGIN_BOX.getstr(3, 3, 12).decode(encoding="utf-8")

        if password == "lang":
            d = {"pl":"en", "en":"pl"}
            CUR_LANG = d[CUR_LANG]

        LOGIN_BOX.addstr(3, 3, "                 ")
        curses.noecho()

    for i in range(31):
        BORDER_BOX.addstr(i+1, 1, " "*110)


    draw_screen()
    draw_reward()
    draw_main()
    draw_status()

    # game stage
    while not game_halve_ended():
        allow_input()


    clear_screen()

    while wir_full() and not os.path.isfile("/tmp/wirokfull"):
        BORDER_BOX.addstr(7, 46, lang("LOCK_BOX.error_desc_full"))
        for i, line in enumerate(alert):
            BORDER_BOX.addstr(1+i, 3, line)
        BORDER_BOX.refresh()

    clear_screen()

    # game stage 2
    while not game_ended():
        allow_input()

    BORDER_BOX.refresh()

    # freeze screen at end
    key = ''
    while key != ord('*'):
        key = STD_SCR.getch()
        BORDER_BOX.refresh()
        draw_screen()
        draw_reward()
        draw_main()
        draw_status()


finally:
    curses.endwin()
